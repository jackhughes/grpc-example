package main

import (
	"gitlab.com/jackhughes/grpc-example/cmd/client/cobra"
)

func main() {
	cobra.Execute()
}
