package cobra

import (
	"context"
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	pb "gitlab.com/jackhughes/grpc-example/proto"
)

func init() {
	rootCmd.AddCommand(reverse)
}

var reverse = &cobra.Command{
	Use:   "reverse [string to reverse]",
	Short: "Reverses a string parameter",
	Long:  "Reverse will take a string and  flip its characters around - hello would become olleh",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		c := viper.Get("Client").(pb.StringOperationsClient)

		sr := &pb.StringRequest{Value: args[0]}
		res, err := c.Reverse(context.Background(), sr)
		if err != nil {
			logrus.Fatalf("error on reversal: %v", err)
		}

		b, err := json.MarshalIndent(res, "", "    ")
		if err != nil {
			logrus.Fatalf("error on json marshalling: %v", err)
		}

		logrus.Infoln(string(b))
	},
}
