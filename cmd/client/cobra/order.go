package cobra

import (
	"context"
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	pb "gitlab.com/jackhughes/grpc-example/proto"
)

func init() {
	rootCmd.AddCommand(order)
}

var order = &cobra.Command{
	Use:   "order [string to order]",
	Short: "Order the characters of a string alphabetically",
	Long:  "Order strings alphabetically, and return the response. This also works for numbers.",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		c := viper.Get("Client").(pb.StringOperationsClient)

		sr := &pb.StringRequest{Value: args[0]}
		res, err := c.Order(context.Background(), sr)
		if err != nil {
			logrus.Fatalf("error on order lookup: %v", err)
		}

		b, err := json.MarshalIndent(res, "", "    ")
		if err != nil {
			logrus.Fatalf("error on json marshalling: %v", err)
		}

		logrus.Infoln(string(b))
	},
}
