package cobra

import (
	"fmt"
	"net"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"google.golang.org/grpc"

	pb "gitlab.com/jackhughes/grpc-example/proto"
)

// Host is the gRPC server host address
var Host string

// Port is the gRPC port
var Port string

const (
	// ProductionAddress default
	ProductionAddress = "34.77.88.249"
	// ProductionPort default
	ProductionPort = "60000"
)

func init() {
	rootCmd.PersistentFlags().StringVarP(&Host, "address", "a", ProductionAddress, "the host to connect to")
	rootCmd.PersistentFlags().StringVarP(&Port, "port", "p", ProductionPort, "the port to connect to")

	viper.BindPFlag("address", rootCmd.PersistentFlags().Lookup("address"))
	viper.BindPFlag("port", rootCmd.PersistentFlags().Lookup("port"))

	rootCmd.MarkFlagRequired("host")
	rootCmd.MarkFlagRequired("port")
}

var rootCmd = &cobra.Command{
	Use:   "strops [subcommand]",
	Short: "The root command for strops",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		host := viper.Get("address").(string)
		port := viper.Get("port").(string)
		addr := net.JoinHostPort(host, port)

		conn, err := grpc.Dial(addr, grpc.WithInsecure())
		if err != nil {
			logrus.Fatalf("could not connect: %v", err)
		}

		c := pb.NewStringOperationsClient(conn)

		viper.SetDefault("Client", c)
		viper.SetDefault("Connection", conn)
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		conn := viper.Get("Connection").(*grpc.ClientConn)
		conn.Close()
	},
}

// Execute the root entrypoint
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
