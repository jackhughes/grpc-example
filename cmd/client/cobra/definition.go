package cobra

import (
	"context"
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	pb "gitlab.com/jackhughes/grpc-example/proto"
)

func init() {
	rootCmd.AddCommand(def)
}

var def = &cobra.Command{
	Use:   "define [string to define]",
	Short: "Define a string against https://owlbot.info/. Get commonly following words from https://api.datamuse.com/",
	Long:  "Define a string against the owlbot.info API. Common words tending to follow the original string will be returned from the datamuse API.",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		c := viper.Get("Client").(pb.StringOperationsClient)
		dr := &pb.DefinitionRequest{Value: args[0]}

		res, err := c.Definition(context.Background(), dr)
		if err != nil {
			logrus.Fatalf("error on definition lookup: %v", err)
		}

		b, err := json.MarshalIndent(res, "", "    ")
		if err != nil {
			logrus.Fatalf("error on json marshalling: %v", err)
		}

		logrus.Infoln(string(b))
	},
}
