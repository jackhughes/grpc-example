package main

import (
	"context"
	"net"
	"sort"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/jackhughes/grpc-example/cmd/server/api"
	pb "gitlab.com/jackhughes/grpc-example/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/peer"
)

const (
	// Definition the definition the definition operation
	Definition = "definition"

	// Order the order operation
	Order = "order"

	// Reverse the reverse operation
	Reverse = "reverse"
)

// StringOperationsServer the server struct
type StringOperationsServer struct {
	Config *Config
}

// Reverse a string
func (s StringOperationsServer) Reverse(ctx context.Context, req *pb.StringRequest) (*pb.StringResponse, error) {
	logAddress(ctx, Reverse)
	res := &pb.StringResponse{}

	rns := []rune(req.Value)
	for i, j := 0, len(rns)-1; i < j; i, j = i+1, j-1 {
		rns[i], rns[j] = rns[j], rns[i]
	}

	res.Value = string(rns)

	return res, nil
}

// Order a string's characters
func (s StringOperationsServer) Order(ctx context.Context, req *pb.StringRequest) (*pb.StringResponse, error) {
	logAddress(ctx, Order)
	res := &pb.StringResponse{}

	str := strings.Split(req.Value, "")
	sort.Strings(str)

	res.Value = strings.Join(str, "")

	return res, nil
}

// Definition retrieves the definition of a string value from multiple sources concurrently
func (s StringOperationsServer) Definition(ctx context.Context, req *pb.DefinitionRequest) (*pb.DefinitionResponse, error) {
	logAddress(ctx, Definition)
	resp := &pb.DefinitionResponse{}

	dr := &api.Definition{}
	dr.WaitGroup.Add(2)

	go api.GetOwlDefinition(s.Config.OwlURL+req.GetValue(), s.Config.OwlToken, dr)
	go api.GetCommonNextWords(s.Config.DatamuseURL+req.GetValue(), dr)

	dr.WaitGroup.Wait()

	resp.Definition = dr.Definition
	resp.CommonNextWords = dr.CommonNextWords

	return resp, nil
}

// main, application entry point
func main() {
	cfg, err := NewServerConfig()
	if err != nil {
		logrus.Fatalf("could not load server configuration: %v", err)
	}

	lis, err := net.Listen("tcp", cfg.Port)
	if err != nil {
		logrus.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterStringOperationsServer(s, &StringOperationsServer{Config: cfg})

	logrus.Info("starting server...")

	if err := s.Serve(lis); err != nil {
		logrus.Fatalf("failed to serve: %v", err)
	}
}

// logAddress of the client connecting from the context
func logAddress(ctx context.Context, op string) {
	if peer, ok := peer.FromContext(ctx); ok {
		logrus.Infof("%s request received from: %v", op, peer.Addr)
	} else {
		logrus.Infof("%s request received, could not collect peer address", op)
	}
}
