package main

import "github.com/kelseyhightower/envconfig"

// Config container for configuration
type Config struct {
	Port        string `required:"true"`
	OwlURL      string `split_words:"true" required:"true"`
	OwlToken    string `split_words:"true" required:"true"`
	DatamuseURL string `split_words:"true" required:"true"`
}

// NewServerConfig hydrates the config struct with values from environment variables
func NewServerConfig() (*Config, error) {
	cfg := &Config{}
	err := envconfig.Process("server", cfg)

	return cfg, err
}
