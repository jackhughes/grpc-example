package api

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRelated(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w,
			`[{
				"word": "and",
				"score": 68542
			  },
			  {
				"word": "was",
				"score": 27535
			  },
			  {
				"word": "to",
				"score": 25508
			  }]`)
	}))

	defer ts.Close()

	dr := &Definition{}
	dr.WaitGroup = sync.WaitGroup{}
	dr.Mutex = sync.RWMutex{}

	url := ts.URL
	dr.WaitGroup.Add(1)

	GetCommonNextWords(url, dr)

	dr.WaitGroup.Wait()

	assert.Equal(t, dr.CommonNextWords, []string{"and", "was", "to"})
}
