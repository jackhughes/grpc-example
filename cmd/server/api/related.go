package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"
)

// GetCommonNextWords that follow the initial parameter string
func GetCommonNextWords(url string, dr *Definition) {
	resp, err := http.Get(url)
	if err != nil {
		logrus.Fatalf("error: %v", err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.Errorf("error: %v", err)
	}

	keys := make([]Words, 0)
	if err := json.Unmarshal(body, &keys); err != nil {
		logrus.Errorf("error unmarshalling: %v", err)
	}

	if len(keys) > 0 {
		var s []string

		for _, v := range keys {
			s = append(s, v.Word)
		}

		dr.Mutex.Lock()
		dr.CommonNextWords = s
		dr.Mutex.Unlock()
	}

	dr.WaitGroup.Done()
}
