package api

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDefinition(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w,
			`{
			"definitions": [
			  {
				"type": "noun",
				"definition": "a road vehicle, typically with four wheels, powered by an internal-combustion engine and able to carry a small number of people.",
				"example": "we're going <b>by car</b>",
				"image_url": "https://media.owlbot.info/dictionary/images/uq.jpg.400x400_q85_box-122,0,542,420_crop_detail.jpg"
			  }
			],
			"word": "car",
			"pronunciation": "kär"
		  }`)
	}))

	defer ts.Close()

	dr := &Definition{}
	dr.WaitGroup = sync.WaitGroup{}
	dr.Mutex = sync.RWMutex{}

	url := ts.URL
	dr.WaitGroup.Add(1)

	GetOwlDefinition(url, "xxxx", dr)

	dr.WaitGroup.Wait()

	assert.Equal(t, dr.Definition, "a road vehicle, typically with four wheels, powered by an internal-combustion engine and able to carry a small number of people.")
}
