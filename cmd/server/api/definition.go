package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"
)

// GetOwlDefinition for a word
func GetOwlDefinition(url, tkn string, dr *Definition) {
	clnt := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		logrus.Errorf("error building request: %v", err)
	}

	req.Header.Add("Authorization", "Token "+tkn)

	resp, err := clnt.Do(req)
	if err != nil {
		logrus.Errorf("error performing request: %v", err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.Errorf("error: %v", err)
	}

	or := &OwlResponse{}
	if err := json.Unmarshal(body, or); err != nil {
		logrus.Errorf("error unmarshalling: %v", err)
	}

	if len(or.Definitions) > 0 {
		dr.Mutex.Lock()
		dr.Definition = or.Definitions[0].Definition
		dr.Mutex.Unlock()
	}

	dr.WaitGroup.Done()
}
