package api

import "sync"

// Definition structure
type Definition struct {
	Definition      string
	CommonNextWords []string
	WaitGroup       sync.WaitGroup
	Mutex           sync.RWMutex
}

// Words contains the score and words that commonly proceed the parameter string
type Words struct {
	Word  string `json:"word"`
	Score int64  `json:"score"`
}

// OwlResponse the overall JSON response structure
type OwlResponse struct {
	Definitions   []Definitions `json:"definitions"`
	Word          string        `json:"word"`
	Pronunciation string        `json:"pronunciation"`
}

// Definitions the nested structure containing metadata about a word
type Definitions struct {
	Type       string `json:"type"`
	Definition string `json:"definition"`
	Example    string `json:"example"`
	ImageURL   string `json:"image_url"`
}
