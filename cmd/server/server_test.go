package main

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/magiconair/properties/assert"
	pb "gitlab.com/jackhughes/grpc-example/proto"
)

func TestReverse(t *testing.T) {
	s := StringOperationsServer{}

	tests := []struct {
		in  string
		out string
	}{
		{
			in:  "abc",
			out: "cba",
		},
		{
			in:  "bca",
			out: "acb",
		},
		{
			in:  "abcdefghijklmnopqrstuvwxyz",
			out: "zyxwvutsrqponmlkjihgfedcba",
		},
	}

	for _, tt := range tests {
		req := &pb.StringRequest{Value: tt.in}
		resp, err := s.Reverse(context.Background(), req)
		if err != nil {
			t.Errorf("error on test run: %v", err)
		}
		if resp.Value != tt.out {
			t.Errorf("have %v, was expecting %v", resp.Value, tt.out)
		}
	}
}

func TestOrder(t *testing.T) {
	s := StringOperationsServer{}

	tests := []struct {
		in  string
		out string
	}{
		{
			in:  "abc",
			out: "abc",
		},
		{
			in:  "bca",
			out: "abc",
		},
		{
			in:  "zyxwvutsrqponmlkjihgfedcba",
			out: "abcdefghijklmnopqrstuvwxyz",
		},
		{
			in:  "987654321",
			out: "123456789",
		},
	}

	for _, tt := range tests {
		req := &pb.StringRequest{Value: tt.in}
		resp, err := s.Order(context.Background(), req)
		if err != nil {
			t.Errorf("error on test run: %v", err)
		}
		if resp.Value != tt.out {
			t.Errorf("have %v, was expecting %v", resp.Value, tt.out)
		}
	}
}

func TestDefinition(t *testing.T) {
	owl := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w,
			`{
			"definitions": [
			  {
				"type": "noun",
				"definition": "a road vehicle, typically with four wheels, powered by an internal-combustion engine and able to carry a small number of people.",
				"example": "we're going <b>by car</b>",
				"image_url": "https://media.owlbot.info/dictionary/images/uq.jpg.400x400_q85_box-122,0,542,420_crop_detail.jpg"
			  }
			],
			"word": "car",
			"pronunciation": "kär"
		  }`)
	}))

	dms := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w,
			`[{
				"word": "and",
				"score": 68542
			  },
			  {
				"word": "was",
				"score": 27535
			  },
			  {
				"word": "to",
				"score": 25508
			  }]`)
	}))

	expResp := &pb.DefinitionResponse{
		Definition:      "a road vehicle, typically with four wheels, powered by an internal-combustion engine and able to carry a small number of people.",
		CommonNextWords: []string{"and", "was", "to"},
	}

	s := StringOperationsServer{
		Config: &Config{
			OwlURL:      owl.URL,
			OwlToken:    "xxxxx",
			Port:        ":8282",
			DatamuseURL: dms.URL,
		},
	}

	req := &pb.DefinitionRequest{Value: ""}
	resp, err := s.Definition(context.Background(), req)
	if err != nil {
		t.Errorf("error on test run: %v", err)
	}

	assert.Equal(t, resp, expResp)
}

func BenchmarkOrder(b *testing.B) {
	s := StringOperationsServer{}
	in := "zyxwvutsrqponmlkjihgfedcba"

	req := &pb.StringRequest{Value: in}
	for n := 0; n < b.N; n++ {
		s.Order(context.Background(), req)
	}
}

func BenchmarkReverse(b *testing.B) {
	s := StringOperationsServer{}
	in := "zyxwvutsrqponmlkjihgfedcba"

	req := &pb.StringRequest{Value: in}
	for n := 0; n < b.N; n++ {
		s.Reverse(context.Background(), req)
	}
}
