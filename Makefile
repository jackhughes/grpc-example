.PHONY: build

build: 
	docker build -t registry.gitlab.com/jackhughes/grpc-example:latest -f build/Dockerfile .

build-client:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o strops ./cmd/client

run:
	docker run registry.gitlab.com/jackhughes/grpc-example:latest

login:
	docker login registry.gitlab.com

push: build
	docker push registry.gitlab.com/jackhughes/grpc-example:latest

lint: 
	golint -set_exit_status cmd/... 

vet:
	cd cmd && go vet ./...

fmt: 
	cd cmd && go fmt ./...

deadcode:
	cd cmd/server && deadcode .

misspell:
	cd cmd/ && misspell .

gocyclo:
	cd cmd/ && gocyclo -over 10 .

test:
	@mkdir -p artifacts
	cd cmd/ && go test ./... -race -cover -coverprofile=../artifacts/coverage.out

coverage: test
	go tool cover -html=artifacts/coverage.out -o artifacts/coverage.html

benchmark:
	cd cmd/server && go test -bench=.