## Secret Definitions

For this application, two secrets are needed. The first is the gitlab authentication details for the image registry. In order to apply that secret to the cluster, you will need to generate a personal access token from gitlab. When you have that information, apply it to the cluster like so:

```
kubectl create secret docker-registry gitlab-auth \
--docker-server=https://registry.gitlab.com \
--docker-username=YOUR_USERNAME \
--docker-password=YOUR_PASSWORD \
--docker-email=YOUR_EMAIL 
```

The second secret you will need, is an API access token for: https://owlbot.info/

Apply this to the cluster by using the following yaml format:

```
apiVersion: v1
kind: Secret
metadata:
  name: owl
type: Opaque
data:
  token: xxxxxxxxxx
```

Where `xxxxxxxxxx` is a base64 encoded string. Do this by running `echo -n "token" | base64`